global exit
global string_length
global print_string
global print_char
global print_newline
global string_equals
global read_word
global print_err


%define SYS_WRITE 1
%define SYS_EXIT 60
%define FD_STDOUT 1
%define FD_STDERR 2

section .text
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rax+rdi], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, FD_STDERR
    jmp print

; Поток вывода ошибок
print_err:
    mov rsi, FD_STDERR
print:
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi

    mov rdx, rax
    mov rax, FD_STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    mov rax, SYS_WRITE
    mov rdi, FD_STDOUT
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0

    xor rcx, rcx
    mov r9, 10

    .loop:
    xor rdx, rdx
    div r9
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    inc rcx

    test rax, rax
    jne .loop

    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    inc rsp
    add rsp, rcx

    ret



; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

    .print:
        call print_uint
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
     xor rcx, rcx
    .loop:
        mov al, [rdi + rcx]
        sub al, [rsi + rcx]
        cmp al, 0
        jne .make_false

        cmp byte [rdi + rcx], 0
        je .make_true

        inc rcx
        jmp .loop

    .make_false:
        xor rax, rax
        ret
    .make_true:
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push rax
    mov rsi, rsp
    mov rdx, 1
    syscall

    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx,rcx

    .loop:
        cmp rcx, rsi
        jge .fail

        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi

        cmp al,0x20
        je .skip
        cmp al,0x9
        je .skip
        cmp al,0xA
        je .skip

        mov byte[rdi+rcx],al
        test al,al
        je .ret
        inc rcx
        jmp .loop

    .skip:
        test rcx,rcx
        jz .loop

    .ret:
        mov rax, rdi
        mov rdx, rcx
        ret

    .fail:
        xor rax, rax
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
.read:
    mov cl, [rdi + rdx]

    cmp rcx, '0'
    jl .end
    cmp rcx, '9'
    jg .end

    sub rcx, '0'
    imul rax, 10
    add rax, rcx
    inc rdx

    jmp .read

    .end:
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера [rdi, rsi, rdx]
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
   push rdi
   push rsi
   push rdx
   call string_length
   pop rdx
   pop rsi
   pop rdi
   cmp rax, rdx
   jge .error
   xor rax, rax
   xor rdx, rdx

   .loop:
       mov dl, [rdi + rax]
       mov [rsi + rax], dl
       test dl, dl
       je .end
       inc rax
       jmp .loop

   .end:
       ret

   .error:
       xor rax, rax
       ret


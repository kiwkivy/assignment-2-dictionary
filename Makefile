ASM=nasm
ASMFLAGS=-felf64
LD=ld


program: test

test: *.o
	$(LD) -o $@ $^

main.o: *.asm *.inc
	$(ASM) $(ASMFLAGS) $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) $<

clean:
	rm -f *.o

.PHONY: clean

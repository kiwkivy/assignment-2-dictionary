%include "lib.inc"
%include "words.inc"

%define buffer_size 256
%define label_size 8

section .rodata
    not_found_err: db "Key not found", 0
    is_not_valid_err: db "Key is not valid", 0
section .data
    buffer: times buffer_size db 0

section .text
global _start
extern find_word

_start:
	mov rdi, buffer
	mov rsi, buffer_size
	call read_word
	cmp rax, 0
	jz .is_not_valid

	push rdx
	mov rdi, rax
	mov rsi, next_elem
	call find_word
	cmp rax, 0
	pop rdx
	jz .not_found

	add rax, label_size
	add rax, rdx
	inc rax
	mov rdi, rax

	call print_string
	call print_newline

	xor rdi, rdi
	call exit

	.is_not_valid:
		mov rdi, is_not_valid_err
		jmp .err_exit

	.not_found:
		mov rdi, not_found_err
	.err_exit:
        	call print_err


		call print_newline
		mov rdi, 1
		call exit





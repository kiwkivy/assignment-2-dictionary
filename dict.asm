section .text
global find_word
extern string_equals

find_word:
	push r10
	push r11
	mov r10, rdi
	mov r11, rsi

.loop:
	add r11, 8
	mov rsi, r11
	mov rdi, r10
	call string_equals
	sub r11, 8
	test rax, rax
	jnz .word_found
	mov r11, [r11]
	test r11, r11
	jnz .loop

.word_not_found:
	xor rax, rax
	pop r11
	pop r10
	ret

.word_found:
	mov rax, r11
	pop r11
	pop r10
	ret
